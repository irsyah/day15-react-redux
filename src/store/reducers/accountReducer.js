// function reducer
// menerima 2 parameter: state dan action
// action berupa object dengan key "type" dan "payload"
const accountReducer = (state = 500000, action) => {
    switch (action.type) {
        case "WITHDRAW":
            return state - action.payload;
            break;
        case "DEPOSIT":
            return state + action.payload;
            break;
    
        default:
            return state;
            break;
    }
}

export default accountReducer;