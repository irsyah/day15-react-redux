import { combineReducers } from "redux";
import accountReducer from './accountReducer';
import listTransactionReducer from './listTransactionReducer';
import todoReducer from './todoReducer';

const reducers = combineReducers({
    balance: accountReducer,
    transactions: listTransactionReducer,
    todos: todoReducer
})

export default reducers;