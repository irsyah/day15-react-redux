const todoReducer = (state = [], action) => {
    switch (action.type) {
        case "SET_TODO":
            return action.payload;
            break;
        case "DELETE_TODO":
            return state.filter(todo => {
                if (todo.id !== action.payload) {
                    return todo;
                }
            })
            break;
        default:
            return state;
            break;
    }
}

export default todoReducer;