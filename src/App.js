import Balance from "./components/Balance";
import Todo from "./components/Todo";
import Transaction from "./components/Transaction";


function App() {
  return (
    <div>
      <Balance />
      <Transaction />
      <Todo />
    </div>
  );
}

export default App;
