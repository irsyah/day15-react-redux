import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

function Transaction() {

  const balance = useSelector(state => state.balance);
  const [ inputMoney, setInputMoney ] = useState(0);

  const dispatch = useDispatch();

  const doDeposit = () => {
    dispatch({ type: "DEPOSIT", payload: Number(inputMoney )});
    reset();
  }

  const doWithdraw = () => {
    dispatch({ type: "WITHDRAW", payload: Number(inputMoney)});
    reset();
  }

  const reset = () => {
    setInputMoney(0);
  }


  return (
    <div>
        <input value={inputMoney} type="text" onChange={(e) => setInputMoney(e.target.value)}/>
        <button onClick={doDeposit}>DEPOSIT</button>
        <button onClick={doWithdraw}>WITHDRAW</button>
    </div>
  )
}

export default Transaction