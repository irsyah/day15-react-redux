import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';

function Todo() {

//   const [ todos, setTodos ] = useState([])

  const todos = useSelector(state => state.todos); // mengambil data state yang ada di store
  const dispatch = useDispatch();

  const fetchTodo = async () => {
    try {
        const response = await fetch(`https://jsonplaceholder.typicode.com/todos`);
        if (response.ok) {
            const data = await response.json();
            // setTodos(data)
            dispatch({ type: "SET_TODO", payload: data })
        }
    } catch (err) {
        console.log(err);
    }
  }  

  useEffect(() => {
    fetchTodo();
  }, [])

  const removeTodo = async (id) => {
    try {   
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: 'DELETE',
          });
        
        if (response.ok) {
            // setTodos(currState => {
            //     return currState.filter(state => {
            //         if (state.id !== id) {
            //             return state;
            //         }
            //     })
            // })
            dispatch({ type: "DELETE_TODO", payload: id })
        }
    } catch (err) {
        console.log(err);
    }
  }

  return (
    <div>
        { todos.map(todo => (
            <div key={todo.id}>
                <h3>{todo.title}</h3>
                <button onClick={() => removeTodo(todo.id)}>Delete</button>
            </div>
        ))}
    </div>
  )
}

export default Todo