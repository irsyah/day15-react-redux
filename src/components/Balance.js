import React, { useState } from 'react';
import { useSelector } from 'react-redux';

function Balance() {
  // const [ balance, setBalance ] = useState(500000);
  const balance = useSelector(state => state.balance);
  const listTrans = useSelector(state => state.transactions)

  return (
    <div>
        <h1>Balance: {balance.toLocaleString("id-ID", {style: "currency", currency: "IDR"})}</h1>
    </div>
  )
}

export default Balance